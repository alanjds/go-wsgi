#include "Python.h"
#include "cStringIO.h"
#include "_cgo_export.h"

static PyObject*
gowsgi_write(PyObject *self, PyObject *args)
{
	int slot, n, written;
	char *buf, *err = NULL;
	slot = (int)PyInt_AsLong(self);
	if (slot == -1 && PyErr_Occurred()) {
		return NULL;
	}
	if (!PyArg_ParseTuple(args, "s#:write", &buf, &n))
		return NULL;
	Py_BEGIN_ALLOW_THREADS
	written = goWrite(slot, buf, n, &err);
	Py_END_ALLOW_THREADS
	if (written != n) {
		PyErr_SetString(PyExc_IOError, err);
		free(err);
		return NULL;
	} else if (err != NULL) {
		/* An error was returned, but all the bytes were written, ignore it. */
		free(err);
	}
	Py_RETURN_NONE;
}

static PyMethodDef write_def = {
	"write",
	gowsgi_write,
	METH_VARARGS,
	"WSGI write callable"
};

static PyObject*
gowsgi_start_response(PyObject *self, PyObject *args)
{
	int slot;
	PyObject *response_headers = NULL;
	PyObject *exc_type = NULL, *exc_val = NULL, *exc_tb = NULL;
	char *status, *rest;
	int code = -1;
	Py_ssize_t i, len;
	slot = (int)PyInt_AsLong(self);
	if (slot == -1 && PyErr_Occurred()) {
		return NULL;
	}
	if (!PyArg_ParseTuple(args, "sO!|(OOO):start_response", &status, &PyList_Type, &response_headers, &exc_type, &exc_val, &exc_tb))
		return NULL;
	if (exc_type != NULL && goHeaderWritten(slot) != 0) {
		Py_INCREF(exc_type);
		Py_XINCREF(exc_val);
		Py_XINCREF(exc_tb);
		PyErr_Restore(exc_type, exc_val, exc_tb);
		return NULL;
	}
	code = (int)strtol(status, &rest, 10);
	if (rest == status || *rest != ' ' || code < 100 || code > 999) {
		PyErr_SetString(PyExc_ValueError, "bad status code provided to start_response");
		return NULL;
	}
	goSetStatus(slot, code);
	len = PyList_Size(response_headers);
	for (i = 0; i < len; i++) {
		PyObject *t, *k, *v;
		char *ks, *vs;
		Py_ssize_t klen, vlen;
		t = PyList_GET_ITEM(response_headers, i);
		if (!PyTuple_Check(t) || PyTuple_GET_SIZE(t) != 2) {
			PyErr_SetString(PyExc_TypeError, "start_response's response_headers must be a list of 2-tuples of strings");
			return NULL;
		}
		k = PyTuple_GET_ITEM(t, 0);
		v = PyTuple_GET_ITEM(t, 1);
		if (!PyString_Check(k) || !PyString_Check(v)) {
			PyErr_SetString(PyExc_TypeError, "start_response's response_headers must be a list of 2-tuples of strings");
			return NULL;
		}
		if (PyString_AsStringAndSize(k, &ks, &klen) < 0) {
			return NULL;
		}
		if (PyString_AsStringAndSize(v, &vs, &vlen) < 0) {
			return NULL;
		}
		goAddHeader(slot, ks, klen, vs, vlen);
	}
	return PyCFunction_New(&write_def, self);
}

static PyMethodDef start_response_def = {
	"start_response",
	gowsgi_start_response,
	METH_VARARGS,
	"WSGI start_response callable"
};

PyObject*
new_start_response(long slot)
{
	PyObject *fn, *self;
	self = PyInt_FromLong(slot);
	if (self == NULL)
		return NULL;
	fn = PyCFunction_New(&start_response_def, self);
	Py_DECREF(self);
	return fn;
}

typedef struct {
	PyObject_HEAD
	long slot;
} RequestReader;

static PyObject*
reader_read_all(long slot)
{
	PyObject *ret = NULL;
	char *s = NULL, *err = NULL;
	int n;
	Py_BEGIN_ALLOW_THREADS
	n = goReadAll(slot, &s, &err);
	Py_END_ALLOW_THREADS
	if (err != NULL) {
		PyErr_SetString(PyExc_IOError, err);
		free(err);
		if (s != NULL) {
			free(s);
		}
		return NULL;
	}
	if (s == NULL) {
		PyErr_SetString(PyExc_IOError, "WSGI Request Reader: Can't happen");
		return NULL;
	}
	ret = PyString_FromStringAndSize(s, n);
	free(s);
	return ret;
}

static PyObject*
reader_read_max(long slot, int max)
{
	PyObject *ret = NULL;
	char *s = NULL, *err = NULL;
	int n;
	ret = PyString_FromStringAndSize(NULL, max);
	if (ret == NULL)
		return NULL;
	s = PyString_AsString(ret);
	if (s == NULL) {
		Py_DECREF(ret);
		return NULL;
	}
	Py_BEGIN_ALLOW_THREADS
	n = goReadN(slot, s, max, &err);
	Py_END_ALLOW_THREADS
	if (err != NULL) {
		PyErr_SetString(PyExc_IOError, err);
		free(err);
		Py_DECREF(ret);
		return NULL;
	}
	if (_PyString_Resize(&ret, (Py_ssize_t)n) != 0)
		return NULL;
	return ret;
}

static PyObject*
reader_read(RequestReader *self, PyObject *args)
{
	int max = -1;
	if (!PyArg_ParseTuple(args, "|i:RequestReader.read", &max))
		return NULL;
	if (max < 0)
		return reader_read_all(self->slot);
	return reader_read_max(self->slot, max);
}

static PyObject*
reader_readline_all(long slot)
{
	PyObject *ret = NULL;
	char *s = NULL, *err = NULL;
	int n;
	Py_BEGIN_ALLOW_THREADS
	n = goReadLineAll(slot, &s, &err);
	Py_END_ALLOW_THREADS
	if (err != NULL) {
		PyErr_SetString(PyExc_IOError, err);
		free(err);
		if (s != NULL) {
			free(s);
		}
		return NULL;
	}
	if (s == NULL) {
		PyErr_SetString(PyExc_IOError, "WSGI Request Reader: Can't happen");
		return NULL;
	}
	ret = PyString_FromStringAndSize(s, n);
	free(s);
	return ret;
}

static PyObject*
reader_readline_max(long slot, int max)
{
	PyObject *ret = NULL;
	char *s = NULL, *err = NULL;
	int n;
	ret = PyString_FromStringAndSize(NULL, max);
	if (ret == NULL)
		return NULL;
	s = PyString_AsString(ret);
	if (s == NULL) {
		Py_DECREF(ret);
		return NULL;
	}
	Py_BEGIN_ALLOW_THREADS
	n = goReadLineN(slot, s, max, &err);
	Py_END_ALLOW_THREADS
	if (err != NULL) {
		PyErr_SetString(PyExc_IOError, err);
		free(err);
		Py_DECREF(ret);
		return NULL;
	}
	if (_PyString_Resize(&ret, (Py_ssize_t)n) != 0)
		return NULL;
	return ret;
}

static PyObject*
reader_readline(RequestReader *self, PyObject *args)
{
	int max = -1;
	if (!PyArg_ParseTuple(args, "|i:RequestReader.readline", &max))
		return NULL;
	if (max < 0)
		return reader_readline_all(self->slot);
	return reader_readline_max(self->slot, max);
}

static PyObject*
reader_readlines(RequestReader *self, PyObject *args)
{
	PyObject *empty =NULL;
	PyObject *list = NULL;
	PyObject *line = NULL;
	/* The hint is ignored. */
	int hint = -1;
	if (!PyArg_ParseTuple(args, "|i:RequestReader.readlines", &hint))
		return NULL;
	empty = PyTuple_New(0);
	if (empty == NULL)
		goto exception;
	list = PyList_New(0);
	if (list == NULL)
		goto exception;
	for (;;) {
		line = reader_readline(self, empty);
		if (line == NULL)
			goto exception;
		if (PyString_GET_SIZE(line) == 0) {
			Py_DECREF(line);
			line = NULL;
			break;
		}
		if (PyList_Append(list, line) < 0)
			goto exception;
		Py_DECREF(line);
	}
	Py_DECREF(empty);
	return list;
exception:
	Py_XDECREF(empty);
	Py_XDECREF(list);
	Py_XDECREF(line);
	return NULL;
}

static PyObject*
reader_iter(PyObject *self)
{
	Py_INCREF(self);
	return self;
}

static PyObject*
reader_next(RequestReader *self)
{
	PyObject *line = reader_readline_all(self->slot);
	if (line == NULL)
		return NULL;
	if (PyString_GET_SIZE(line) == 0) {
		Py_DECREF(line);
		return NULL;
	}
	return line;
}

static PyMethodDef RequestReaderMethods[] = {
	{"read", (PyCFunction)reader_read, METH_VARARGS, NULL},
	{"readline", (PyCFunction)reader_readline, METH_VARARGS, NULL},
	{"readlines", (PyCFunction)reader_readlines, METH_VARARGS, NULL},
	{NULL},
};

static PyTypeObject RequestReaderType = {
	PyObject_HEAD_INIT(NULL)
	0,                          /*ob_size*/
	"WSGIRequestReader",        /*tp_name*/
	sizeof(RequestReader),      /*tp_basicsize*/
	0,                          /*tp_itemsize*/
	0,                          /*tp_dealloc*/
	0,                          /*tp_print*/
	0,                          /*tp_getattr*/
	0,                          /*tp_setattr*/
	0,                          /*tp_compare*/
	0,                          /*tp_repr*/
	0,                          /*tp_as_number*/
	0,                          /*tp_as_sequence*/
	0,                          /*tp_as_mapping*/
	0,                          /*tp_hash */
	0,                          /*tp_call*/
	0,                          /*tp_str*/
	0,                          /*tp_getattro*/
	0,                          /*tp_setattro*/
	0,                          /*tp_as_buffer*/
	Py_TPFLAGS_DEFAULT,         /*tp_flags*/
	"WSGI RequestReader Class", /*tp_doc*/
	0,		                    /* tp_traverse */
	0,		                    /* tp_clear */
	0,		                    /* tp_richcompare */
	0,		                    /* tp_weaklistoffset */
	reader_iter,		        /* tp_iter */
	(iternextfunc)reader_next,  /* tp_iternext */
	RequestReaderMethods,       /* tp_methods */
};

typedef struct {
	PyObject_HEAD
	long slot;
} ErrorWriter;

static PyObject*
error_flush(PyObject *self, PyObject *args) {
	Py_RETURN_NONE;
}

static PyObject*
error_write(ErrorWriter *self, PyObject *args) {
	int n;
	char *buf;
	if (!PyArg_ParseTuple(args, "s#:write", &buf, &n))
		return NULL;
	Py_BEGIN_ALLOW_THREADS
	goLogError(self->slot, buf, n);
	Py_END_ALLOW_THREADS
	Py_RETURN_NONE;
}

static PyObject*
error_writelines(ErrorWriter *self, PyObject *args) {
	PyObject *obj = NULL;
	PyObject *iter = NULL;
	PyObject *item = NULL;
	PyObject *wargs = NULL;
	PyObject *ret = NULL;
	if (!PyArg_ParseTuple(args, "O:writelines", &obj))
		return NULL;
	iter = PyObject_GetIter(obj);
	if (iter == NULL)
		goto exception;
	for (;;) {
		item = PyIter_Next(iter);
		if (item == NULL)
			break; /* not necessarily an exception */
		wargs = Py_BuildValue("(O)", item);
		if (wargs == NULL)
			goto exception;
		ret = error_write(self, wargs);
		if (ret == NULL)
			goto exception;
		Py_DECREF(ret);
		ret = NULL;
		Py_DECREF(wargs);
		wargs = NULL;
		Py_DECREF(item);
		item = NULL;
	}
	if (PyErr_Occurred() != NULL) {
		goto exception;
	}
	Py_DECREF(iter);
	Py_DECREF(obj);
	Py_RETURN_NONE;
exception:
	Py_XDECREF(obj);
	Py_XDECREF(iter);
	Py_XDECREF(item);
	Py_XDECREF(wargs);
	Py_XDECREF(ret);
	return NULL;
}

static PyMethodDef ErrorWriterMethods[] = {
	{"flush", error_flush, METH_VARARGS, NULL},
	{"write", (PyCFunction)error_write, METH_VARARGS, NULL},
	{"writelines", (PyCFunction)error_writelines, METH_VARARGS, NULL},
	{NULL},
};

static PyTypeObject ErrorWriterType = {
	PyObject_HEAD_INIT(NULL)
	0,                          /*ob_size*/
	"WSGIErrorWriter",          /*tp_name*/
	sizeof(ErrorWriter),        /*tp_basicsize*/
	0,                          /*tp_itemsize*/
	0,                          /*tp_dealloc*/
	0,                          /*tp_print*/
	0,                          /*tp_getattr*/
	0,                          /*tp_setattr*/
	0,                          /*tp_compare*/
	0,                          /*tp_repr*/
	0,                          /*tp_as_number*/
	0,                          /*tp_as_sequence*/
	0,                          /*tp_as_mapping*/
	0,                          /*tp_hash */
	0,                          /*tp_call*/
	0,                          /*tp_str*/
	0,                          /*tp_getattro*/
	0,                          /*tp_setattro*/
	0,                          /*tp_as_buffer*/
	Py_TPFLAGS_DEFAULT,         /*tp_flags*/
	"WSGI ErrorWriter Class",   /*tp_doc*/
	0,		                    /* tp_traverse */
	0,		                    /* tp_clear */
	0,		                    /* tp_richcompare */
	0,		                    /* tp_weaklistoffset */
	0,		                    /* tp_iter */
	0,                          /* tp_iternext */
	ErrorWriterMethods,         /* tp_methods */
};

int
initwsgi(void) 
{
	PycString_IMPORT;
	RequestReaderType.tp_new = PyType_GenericNew;
	if (PyType_Ready(&RequestReaderType) < 0)
		return 0;
	Py_INCREF(&RequestReaderType);
	ErrorWriterType.tp_new = PyType_GenericNew;
	if (PyType_Ready(&ErrorWriterType) < 0)
		return 0;
	Py_INCREF(&ErrorWriterType);
	return 1;
}

PyObject*
new_request_reader(long slot)
{
	RequestReader *self;
	self = PyObject_New(RequestReader, &RequestReaderType);
	if (self == NULL)
		return NULL;
	self->slot = slot;
	return (PyObject*)self;
}

PyObject*
new_error_writer(long slot)
{
	ErrorWriter *self;
	self = PyObject_New(ErrorWriter, &ErrorWriterType);
	if (self == NULL)
		return NULL;
	self->slot = slot;
	return (PyObject*)self;
}

PyObject*
traceback_to_string(PyObject *tb)
{
	PyObject *buf;
	PyObject *s = NULL;
	buf = PycStringIO->NewOutput(512);
	if (buf == NULL)
		return NULL;
	if (PyTraceBack_Print(tb, buf) == 0)
		s = PycStringIO->cgetvalue(buf);
	Py_DECREF(buf);
	return s;
}

PyTypeObject*
pyStringType(void)
{
	return &PyString_Type;
}

