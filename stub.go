// +build oracle

package wsgi

import (
	"log"
	"net/http"
)

// This file has stubbed versions of the API in wsgi.go.
// The point is to make the Go oracle work on packages that use this one,
// even though oracle can't build this package because of cgo.

var MaxThreads = 20
var PythonPath []string = nil
var IgnoreEnvironment = true

type Handler struct {
	ErrorLog   *log.Logger
	otherStuff bool
}

func Initialize() error {
	return nil
}

func Finalize() {
}

func NewHandler(module, appExpr string, env map[string]string) (*Handler, error) {
	return nil, nil
}
func (h *Handler) Close() {
}
func (h *Handler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
}

type PythonError struct {
	Exception string
	Traceback string
}

func (e PythonError) Error() string {
	// Python itself prints the exception after the
	// traceback, so we follow suit.
	return "Python exception: " + e.Traceback + e.Exception
}
