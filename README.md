Package wsgi enables embedding Python web applications in Go programs.
======================================================================

This package makes WSGI applications usable via the
http.Handler interface. See godoc for details.

[![GoDoc](https://godoc.org/bitbucket.org/classroomsystems/wsgi?status.png)](https://godoc.org/bitbucket.org/classroomsystems/wsgi)

Installation
------------

	go get bitbucket.org/classroomsystems/wsgi


Example
-------

There is a simple, working WSGI server in
[wsgi-server/main.go](wsgi-server/main.go).
