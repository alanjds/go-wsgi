package wsgi

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

func init() {
	// This should get us using the local Python library,
	// but able to import from the current directory.
	IgnoreEnvironment = false
	os.Setenv("PYTHONPATH", ".")
}

type responseCheck func(*testing.T, testCase, *httptest.ResponseRecorder, *bytes.Buffer)

type testCase struct {
	app    string
	method string
	urlStr string
	body   io.Reader
	checks []responseCheck
}

var tests = []testCase{
	// We don't care about the type of app.
	{"hello_func", "GET", "/", eofReader{},
		[]responseCheck{statusIs(200), bodyIs("Hello, World!\n"), errorIs("")},
	},
	{"HelloClass", "GET", "/", eofReader{},
		[]responseCheck{statusIs(200), bodyIs("Hello, World!\n"), errorIs("")},
	},
	{"HelloOldClass", "GET", "/", eofReader{},
		[]responseCheck{statusIs(200), bodyIs("Hello, World!\n"), errorIs("")},
	},
	{"HelloObj()", "GET", "/", eofReader{},
		[]responseCheck{statusIs(200), bodyIs("Hello, World!\n"), errorIs("")},
	},
	{"remote_addr", "GET", "/", eofReader{},
		[]responseCheck{statusIs(200), bodyIs("1.2.3.4"), errorIs("")},
	},
	{"bad_args", "GET", "/", eofReader{},
		[]responseCheck{statusIs(500), errorContains("'NoneType' object is not callable")},
	},
	{"misbehave", "GET", "/bad_call", eofReader{},
		[]responseCheck{statusIs(500), errorContains("start_response() takes no keyword arguments")},
	},
	{"misbehave", "GET", "/bad_status", eofReader{},
		[]responseCheck{statusIs(500), errorContains("bad status code provided to start_response")},
	},
	{"misbehave", "GET", "/bad_headers", eofReader{},
		[]responseCheck{statusIs(500), errorContains("start_response's response_headers must be a list of 2-tuples of strings")},
	},
	{"misbehave", "GET", "/bad_iter", eofReader{},
		[]responseCheck{statusIs(500), errorContains("object is not iterable")},
	},
	{"misbehave", "GET", "/bad_string", eofReader{},
		[]responseCheck{statusIs(500), errorContains("can only write byte strings, not unicode")},
	},
	{"misbehave", "GET", "/no_start_response", eofReader{},
		[]responseCheck{statusIs(500), errorContains("start_response not called before writing data")},
	},
	{"write_errors", "GET", "/", eofReader{},
		[]responseCheck{statusIs(200), bodyIs("OK"), errorIs("plain write\nwritelines 1\nwritelines 2\nwritelines 3\n")},
	},
	{"raiser", "GET", "/right_away", eofReader{},
		[]responseCheck{statusIs(500), bodyContains("Internal error"), errorContains("assert False")},
	},
	{"raiser", "GET", "/after_start_response", eofReader{},
		[]responseCheck{statusIs(500), bodyContains("Internal error"), errorContains("assert False")},
	},
	{"raiser", "GET", "/pass_to_start_response", eofReader{},
		[]responseCheck{statusIs(503), bodyIs("OK"), errorIs("")},
	},
	{"raiser", "GET", "/pass_to_start_response2", eofReader{},
		[]responseCheck{statusIs(503), bodyIs("OK"), errorIs("")},
	},
	{"raiser", "GET", "/pass_to_start_response3", eofReader{},
		[]responseCheck{statusIs(200), bodyIs("NOT OK"), errorContains("assert False")},
	},
	{"raiser", "GET", "/after_data", eofReader{},
		[]responseCheck{statusIs(200), bodyIs("NOT OK"), errorContains("assert False")},
	},
	{"no_content", "GET", "/", eofReader{},
		[]responseCheck{statusIs(418), bodyIs(""), errorIs("")},
	},
	{"set_headers", "GET", "/", eofReader{},
		[]responseCheck{
			statusIs(200), bodyIs(""), errorIs(""),
			hasHeader("X-Header-Foo", "Foo is the value"),
			hasHeader("X-Header-Bar", "Bar is the value"),
			hasHeader("X-Header-Bar", "Bar has another value"),
		},
	},
}

func (c testCase) String() string {
	return fmt.Sprintf("%s %s%s", c.method, c.app, c.urlStr)
}

func statusIs(s int) responseCheck {
	return func(t *testing.T, c testCase, r *httptest.ResponseRecorder, _ *bytes.Buffer) {
		if r.Code != s {
			t.Errorf("%v: Unexpected response status: %d", c, r.Code)
		}
	}
}

func bodyIs(s string) responseCheck {
	return func(t *testing.T, c testCase, r *httptest.ResponseRecorder, _ *bytes.Buffer) {
		if r.Body.String() != s {
			t.Errorf("%v: Unexpected body: %q", c, r.Body.String())
		}
	}
}

func bodyContains(s string) responseCheck {
	return func(t *testing.T, c testCase, r *httptest.ResponseRecorder, _ *bytes.Buffer) {
		if !strings.Contains(r.Body.String(), s) {
			t.Errorf("%v: Unexpected body: %q", c, r.Body.String())
		}
	}
}

func hasHeader(key, value string) responseCheck {
	key = http.CanonicalHeaderKey(key)
	return func(t *testing.T, c testCase, r *httptest.ResponseRecorder, _ *bytes.Buffer) {
		vs := r.Header()[key]
		switch len(vs) {
		case 0:
			t.Errorf("%v: Missing header: %q", c, key)
		case 1:
			if vs[0] != value {
				t.Errorf("%v: Bad value for header: %q; got %q expecting %q", c, key, vs[0], value)
			}
		default:
			for i := range vs {
				if vs[i] == value {
					return
				}
			}
			t.Errorf("%v: %d %q headers, but none has value %q", c, len(vs), key, value)
		}
	}
}

func errorIs(s string) responseCheck {
	return func(t *testing.T, c testCase, _ *httptest.ResponseRecorder, err *bytes.Buffer) {
		if err.String() != s {
			t.Errorf("%v: Unexpected error log: %q", c, err.String())
		}
	}
}

func errorContains(s string) responseCheck {
	return func(t *testing.T, c testCase, _ *httptest.ResponseRecorder, err *bytes.Buffer) {
		if !strings.Contains(err.String(), s) {
			t.Errorf("%v: Unexpected error log: %q", c, err.String())
		}
	}
}

func TestWSGI(t *testing.T) {
	err := Initialize()
	if err != nil {
		t.Errorf("initialization error: %v", err)
		return
	}
	defer Finalize()
	_, err = NewHandler("testapps", "no_such_app", nil)
	if err == nil {
		t.Errorf("No error loading nonexistent app.")
	}
	var buf bytes.Buffer
	for _, tc := range tests {
		h, err := NewHandler("testapps", tc.app, nil)
		if err != nil {
			t.Errorf("%v: Error loading test app: %v", tc, err)
			continue
		}
		h.ErrorLog = log.New(&buf, "", 0)
		req, err := http.NewRequest(tc.method, "http://www.example.com"+tc.urlStr, tc.body)
		if err != nil {
			t.Errorf("%v: Couldn't create request", tc)
			continue
		}
		req.RemoteAddr = "1.2.3.4:12345"
		w := httptest.NewRecorder()
		(&buf).Reset()
		h.ServeHTTP(w, req)
		for _, check := range tc.checks {
			check(t, tc, w, &buf)
		}
	}
}

var echoHandlers = []struct{ app, path string }{
	{"echo_read", "/read"},
	{"echo_read", "/readN"},
	{"echo_read", "/readline"},
	{"echo_read", "/readlineN"},
	{"echo_read", "/readlines"},
	{"echo_read", "/iter"},
	{"echo_write", "/list"},
	{"echo_write", "/str"},
	{"echo_write", "/callback"},
	{"echo_yield", "/"},
}

func TestEcho(t *testing.T) {
	err := Initialize()
	if err != nil {
		t.Errorf("initialization error: %v", err)
		return
	}
	defer Finalize()
	input := map[string]string{
		"empty":          "",
		"funkyChars":     "a\nb\r\nc\rd\n\x00→\n",
		"100kOneLine":    strings.Repeat("abcdefgh", 12800),
		"100kShortLines": strings.Repeat("abcdefg\n", 12800),
		"100kBlankLines": strings.Repeat("\n", 102400),
	}
	var logBuf bytes.Buffer
	logger := log.New(&logBuf, "", 0)
	for _, tc := range echoHandlers {
		h, err := NewHandler("testapps", tc.app, nil)
		if err != nil {
			t.Errorf("Error loading test app %s: %v", tc.app, err)
		}
		h.ErrorLog = logger
		for k, s := range input {
			req, err := http.NewRequest(
				"POST",
				"http://www.example.com"+tc.path,
				strings.NewReader(s),
			)
			if err != nil {
				t.Errorf("%s%s(%s): Couldn't create request", tc.app, tc.path, k)
				continue
			}
			w := httptest.NewRecorder()
			(&logBuf).Reset()
			h.ServeHTTP(w, req)
			if w.Code != 200 {
				t.Errorf("%s%s(%s): Bad response code, got %d expecting 200", tc.app, tc.path, k, w.Code)
			}
			if logBuf.Len() != 0 {
				t.Errorf("%s%s(%s): Unexpected error log: %q", tc.app, tc.path, k, logBuf.String())
			}
			if w.Body.String() != s {
				t.Errorf("%s%s(%s): Unexpected body: %q", tc.app, tc.path, k, w.Body.String())
			}
		}
	}
}

func benchEcho(b *testing.B, app, path, input string) {
	err := Initialize()
	if err != nil {
		b.Errorf("initialization error: %v", err)
		return
	}
	defer Finalize()
	h, err := NewHandler("testapps", app, nil)
	if err != nil {
		b.Errorf("Error loading test app %s: %v", app, err)
		return
	}
	clients := MaxThreads * 10
	base := b.N / clients
	remainder := b.N % clients
	done := make(chan int, 10)
	client := func(n int) {
		for i := 0; i < n; i++ {
			w := httptest.NewRecorder()
			req, err := http.NewRequest(
				"POST",
				"http://www.example.com"+path,
				strings.NewReader(input),
			)
			if err != nil {
				b.Errorf("Error creating request: %v", err)
				break
			}
			h.ServeHTTP(w, req)
			if w.Code != 200 {
				b.Errorf("bad response code, got %d expecting 200", w.Code)
			}
			if w.Body.String() != input {
				b.Errorf("unexpected body: %q", w.Body.String())
			}
		}
		done <- 1
	}
	b.ResetTimer()
	for i := 0; i < clients; i++ {
		if i < remainder {
			go client(base + 1)
		} else {
			go client(base)
		}
	}
	for i := 0; i < clients; i++ {
		<-done
	}

}

var (
	longLine   = strings.Repeat("abcdefgh", 12800)
	shortLines = strings.Repeat("abcdefg\n", 12800)
)

func BenchmarkEchoReadEOF(b *testing.B)        { benchEcho(b, "echo_read", "/read", "") }
func BenchmarkEchoRead(b *testing.B)           { benchEcho(b, "echo_read", "/read", longLine) }
func BenchmarkEchoReadShort(b *testing.B)      { benchEcho(b, "echo_read", "/read", shortLines) }
func BenchmarkEchoReadN(b *testing.B)          { benchEcho(b, "echo_read", "/readN", longLine) }
func BenchmarkEchoReadNShort(b *testing.B)     { benchEcho(b, "echo_read", "/readN", shortLines) }
func BenchmarkEchoReadline(b *testing.B)       { benchEcho(b, "echo_read", "/readline", longLine) }
func BenchmarkEchoReadlineShort(b *testing.B)  { benchEcho(b, "echo_read", "/readline", shortLines) }
func BenchmarkEchoReadlineN(b *testing.B)      { benchEcho(b, "echo_read", "/readlineN", longLine) }
func BenchmarkEchoReadlineNShort(b *testing.B) { benchEcho(b, "echo_read", "/readlineN", shortLines) }
func BenchmarkEchoReadlines(b *testing.B)      { benchEcho(b, "echo_read", "/readlines", longLine) }
func BenchmarkEchoReadlinesShort(b *testing.B) { benchEcho(b, "echo_read", "/readlines", shortLines) }
func BenchmarkEchoReadIter(b *testing.B)       { benchEcho(b, "echo_read", "/iter", longLine) }
func BenchmarkEchoReadIterShort(b *testing.B)  { benchEcho(b, "echo_read", "/iter", shortLines) }

func BenchmarkEchoWriteList(b *testing.B)     { benchEcho(b, "echo_write", "/list", longLine) }
func BenchmarkEchoWriteStr(b *testing.B)      { benchEcho(b, "echo_write", "/str", longLine) }
func BenchmarkEchoWriteCallback(b *testing.B) { benchEcho(b, "echo_write", "/callback", longLine) }
func BenchmarkEchoYield(b *testing.B)         { benchEcho(b, "echo_yield", "/", longLine) }
